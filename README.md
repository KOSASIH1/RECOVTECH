[![CircleCI](https://circleci.com/gh/KOSASIH/RECOVTECH/tree/main.svg?style=svg)](https://circleci.com/gh/KOSASIH/RECOVTECH/tree/main)
[![Documentation Status](https://readthedocs.org/projects/recovtech/badge/?version=latest)](https://recovtech.readthedocs.io/en/latest/?badge=latest)
[![DeepSource](https://deepsource.io/gh/KOSASIH/RECOVTECH.svg/?label=active+issues&show_trend=true&token=6gRqNZAfFzMH-aUJ0aBuzX_k)](https://deepsource.io/gh/KOSASIH/RECOVTECH/?ref=repository-badge)
[![Netlify Status](https://api.netlify.com/api/v1/badges/dc4bc29f-4eb8-4c69-a432-4bffdaafa192/deploy-status)](https://app.netlify.com/sites/recovtech/deploys)
[![Build status](https://ci.appveyor.com/api/projects/status/hrfdv3boa5ynodwb?svg=true)](https://ci.appveyor.com/project/KOSASIH/recovtech)
[![GitHub issues](https://img.shields.io/github/issues/KOSASIH/RECOVTECH)](https://github.com/KOSASIH/RECOVTECH/issues)
[![GitHub license](https://img.shields.io/github/license/KOSASIH/RECOVTECH?style=plastic)](https://github.com/KOSASIH/RECOVTECH/blob/main/LICENSE)
[![GitHub forks](https://img.shields.io/github/forks/KOSASIH/RECOVTECH)](https://github.com/KOSASIH/RECOVTECH/network)
[![GitHub stars](https://img.shields.io/github/stars/KOSASIH/RECOVTECH)](https://github.com/KOSASIH/RECOVTECH/stargazers)
[![Twitter](https://img.shields.io/twitter/url?style=social&url=https%3A%2F%2Fmobile.twitter.com%2FKosasihg88G)](https://twitter.com/intent/tweet?text=Wow:&url=https%3A%2F%2Fmobile.twitter.com%2FKosasihg88G)

# RECOVTECH
Disaster auto recovery data base management system
